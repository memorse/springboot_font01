$(function () {
    getUsers(getSearchData(),"get");


    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getUsers(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getUsers(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getUsers(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getUsers(data,type) {
    MyAjax("/user",data,type,(res)=>{
        USERS=res.data.users
        CURRENT_PAGE=res.data.currentPage
        $("#pageSize").val(res.data.pageSize);
        $("#current_page").val(res.data.currentPage);
        $("#all_page").text(res.data.count);
        COUNT=res.data.count
        packaging(res.data.users)
        loadTeamDataToHtml(res.data.teams)
    })
}
//获取查询条件
function getSearchData() {
    let search={
        name:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        degree:$("#search_degree").val(),
        gender:$('input:radio[name="search_gender"]:checked').val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val(),
        status:$("#search_status").val(),
        team:$("#search_team").val()
    }
    return search
}

function doSearch() {
    getUsers(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,user)=>{
        htmlStr+=`
                <tr>
                <td>${user.id}</td>
                <td>${user.code}</td>
                <td>${user.name}</td>
                <td>${user.gender}</td>
                <td>${user.age}</td>
                <td>${user.groupCode==null?'暂无小组':user.groupCode.teamName}</td>
                <td>${user.degree}</td>
                <td>${user.graduateSchool}</td>
                <td>${user.nativeAddress}</td>
                <td>${user.address}</td>
                <td>${user.phone}</td>
                <td>${user.emergencyPhone}</td>
                <td class="hide-text" style="width: 60px;">${jiami(user.password)}</td>
                <td>${user.enable}</td>
                <td>${user.updateTime}</td>
                <td>${user.createTime}</td>
                <td>
                <div onclick="switchBtn(2);getUserById(${user.id})">修改</div>
                <div onclick="delUser(${user.id})">删除</div>
                ${user.enable==1?
                `<div id="lock"+${user.code} onclick="lockOrUnlock('${user.code}',0)">冻结账户</div>`:
                `<div id="unlock"+${user.code} onclick="lockOrUnlock('${user.code}',1)">解冻账户</div>`
                }
                </td>
</tr>
            `
        // islocked(user.code,(res)=>{
        //     if(res.data==1){
        //         $("#lock"+code).show()
        //         $("#unlock"+code).hide()
        //     }else {
        //         $("#lock"+code).hide()
        //         $("#unlock"+code).show()
        //     }
        // })
    })
    $("#tbody").html(htmlStr)
}

function getUserById(id) {
    MyAjax("/user/"+id,"","get",(res)=>{
        setFormData(res.data)
    })
}

function islocked(code,callback) {
    MyAjax("/user/isAvailable",{userCode:code},"get",(res)=>{
        callback(res)
    })
}

function lockOrUnlock(code, enable) {
    MyAjax("/user/lock",
        JSON.stringify({userName:code,enable:enable}),
        "post",
        (res)=>{
            if(res.data>0)
                getUsers(getSearchData(),"get")
            else {
                alert("请稍后重试！")
            }
        }
    )
}

function showPermission(data) {
    let str=""
    $.each(data,(i,per)=>{
        if (i==data.length-1)
            str+=per.name
        else
            str+=per.name+" | "
    })
    return str
}
//获取表单提交的数据
function getUserFormData() {
    let user={
        id:$("#id").val(),
        name:$("#name").val(),
        code:$("#code").val(),
        gender:getGender(),
        age:$("#age").val(),
        groupCode:{teamCode:$("#group_code").val()},
        degree:$("#degree").val(),
        graduateSchool:$("#graduate_school").val(),
        nativeAddress:$("#native_adress").val(),
        address:$("#address").val(),
        phone:$("#phone").val(),
        emergencyPhone:$("#emergency_phone").val(),
        password:$("#user_password").val(),
        enable:getEnable()
    }
    return user;

}

//删除记录
function delUser(id) {
    if(!confirm("是否删除?"))
        return
    MyAjax("/user/"+id,"","Delete",(res)=>{
        if(res.data.result>0){
            console.log("删除成功")
            getUsers(getSearchData(),"get");
        }else {
            console.log("删除失败")
        }
    })
}

//修改记录
function modify() {
    let user=getUserFormData();
    MyAjax("/user",JSON.stringify(user),"post",(res)=>{
        if(res.data.result>0){
            $("#add_div").hide();
            remove_pointer();
            getUsers(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}

//添加记录
function add() {
    MyAjax("/user",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.data.result>0){
            $("#add_div").hide();
            getUsers(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

function loadTeamDataToHtml(data) {
    let htmlStr="";
    let htmlStr_search=`<option value="all">全部</option>`
    $.each(data,(i,team)=>{
        htmlStr+=`
            <option value="${team.teamCode}">${team.teamName}</option>
        `
        htmlStr_search+=`
            <option value="${team.teamCode}">${team.teamName}</option>
        `
    })
    $("#group_code").html(htmlStr)
    $("#search_team").html(htmlStr_search)
}

//加密
function jiami(str) {
    let result="";
    if(str=='' ||str==null)
        return result;
    for(i=0;i<str.length;i++){
        result+=str[i].replace(str[i],"*")
    }
    return result;
}

//获取性别
function getGender() {
    return $('input:radio[name="gender"]:checked').val();
}

//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}

// //获取爱好
// function getHobby() {
//     let hobyys = $("input:checkbox[name=hobby]");
//     let hobby = "";
//     for (let i = 0; i < hobyys.length; i++) {
//         if (hobyys[i].checked) {
//             hobby += hobyys[i].value + ","
//         }
//     }
//     return hobby;
// }



//重置表单数据为空
function resetFormData() {
        $("#name").val("")
        $("#code").val("")
        $("#user_gender_male").prop("checked","true")
        $("#age").val("")
        $("#group_code").val("")
        $("#degree").val("")
        $("#graduate_school").val("")
        $("#native_adress").val("")
        $("#address").val("")
        $("#phone").val("")
        $("#emergency_phone").val("")
        $("#user_password").val("")
        $("#confirm_password").val("")
        $("#enable").val("")
}

//设置表单数据
function setFormData(user) {

        $("#id").val(user.id)

        $("#name").val(user.name)

        $("#code").val(user.code)

        $("#age").val(user.age)

        $("#graduate_school").val(user.graduateSchool)

        $("#native_adress").val(user.nativeAddress)

        $("#address").val(user.address)

        $("#phone").val(user.phone)

        $("#emergency_phone").val(user.emergencyPhone)

        $("#user_password").val(user.password)

        $("#group_code").val(user.groupCode.teamCode)

        $("#degree").val(user.degree)

        if(user.gender=='男')
            $("#user_gender_male").prop("checked","true")
        else
            $("#user_gender_fmale").prop("checked","true")

        if(user.enable=='0')
            $("#enable_no").prop("checked","true")
        else
            $("#enable_yes").prop("checked","true")
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getUsers(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getUsers(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getUsers(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getUsers(search,"get");
}
