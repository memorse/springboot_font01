var URL="http://localhost:8802";
   function quit() {
        // var login=document.getElementById("logindiv");

        $("#logindiv").hide();
    }
    function do_login() {
        if(verifyUsername()){
            $("#usernameTip").show();
            return;
        }else{
            $("#usernameTip").hide();
        }
        if(verifyPassword()){
            $("#pwdTip").show()
            return;
        }else {
            $("#pwdTip").hide()
        }

        if(!isCode_right){
            $("#code").focus();
            return;
        }
        MyAjax(
            "/login",
            JSON.stringify({
                    username:$("#username").val(),
                    password:$("#password").val()
                }),
            "post",
            (res)=>{
            if(res.code==200){
                sessionStorage.setItem("logindata",JSON.stringify(res.data.access_token))
                $.cookie("token",res.data.tokenHead+res.data.access_token)
                sessionStorage.setItem("userId",$("#username").val())
                quit();
                location.href="/html/manage.html"
            }else{
                alert(res.message);

            }
        })
    }

    function verifyUsername() {
       return  $("#username").val()==""
    } function verifyPassword() {
       return  $("#password").val()==""
   }


   var isCode_right=false;

   // 验证码校验
   $(function () {
// 当鼠标失去焦就开始验证
       $("#code").bind("blur",()=>{
           $.ajax({
                url:URL+"/verifyImageCode",
               type:"post",
                data:{
                   code:$("#code").val()
               },
               xhrFields: {
                   withCredentials: true //允许跨域带Cookie
               },
               success:(res)=>{
                   res=JSON.parse(res)
                   if(res==0){
                       isCode_right=false;
                       $("#code_tip").show();
                   }else {
                       isCode_right=true;
                       $("#code_tip").hide();
                   }
               }})
       })


//全局绑定回车登陆
       $(document).bind("keydown",function (e) {
           if(e.keyCode=="13"){
               do_login();
           }
       })
   })