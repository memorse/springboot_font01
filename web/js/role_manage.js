$(function () {
    getRoles(getSearchData(),"get");


    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getRoles(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getRoles(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getRoles(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getRoles(data,type) {
    MyAjax("/role",{search:JSON.stringify(data)},type,(res)=>{
        USERS=res.roles
        CURRENT_PAGE=res.currentPage
        $("#pageSize").val(res.pageSize);
        $("#current_page").val(res.currentPage);
        $("#all_page").text(res.count);
        COUNT=res.count
        packaging(res.roles)
    })
}

//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val()
    }
    return search
}

function doSearch() {
    getRoles(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,role)=>{
        htmlStr+=`
                <tr>
                <td>${role.id}</td>
                <td>${role.roleCode}</td>
                <td>${role.roleName}</td>
                <td>${role.enable==1?"可用":"不可用"}</td>
                <td>${role.updateTime}</td>
                <td>${role.createTime}</td>
                <td>
                <div onclick="switchBtn(2);getUserById(${role.id})">修改</div>
                <div onclick="delUser(${role.id})">删除</div>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}

//获取表单提交的数据
function getUserFormData() {
    let role={
        id:$("#id").val(),
        roleName:$("#name").val(),
        roleCode:$("#code").val(),
        enable:getEnable()
    }
    return role;

}

//删除记录
function delUser(id) {
    if(!confirm("是否删除?"))
        return
    MyAjax("/role/"+id,"","Delete",(res)=>{
        if(res.result>0){
            console.log("删除成功")
            getRoles(getSearchData(),"get");
        }else {
            console.log("删除失败")
        }
    })
}

//修改记录
function modify() {
    let user=getUserFormData();
    MyAjax("/role",JSON.stringify(user),"post",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            remove_pointer();
            getRoles(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}

//添加记录
function add() {
    MyAjax("/role",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            getRoles(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;


//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}


//重置表单数据为空
function resetFormData() {
        $("#id").val("")
        $("#name").val("")
        $("#code").val("")
        $("#enable").val("")
}

//设置表单数据
function setFormData(role) {

        $("#id").val(role.id)

        $("#name").val(role.roleName)

        $("#code").val(role.roleCode)

        if(role.enable=='0')
            $("#enable_no").prop("checked","true")
        else
            $("#enable_yes").prop("checked","true")
}

function getUserById(id) {
    MyAjax("/role/"+id,"","get",(res)=>{
        setFormData(res)
    })
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getRoles(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getRoles(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getRoles(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getRoles(search,"get");
}
