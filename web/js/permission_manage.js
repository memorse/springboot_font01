$(function () {
    getPermissions(getSearchData(),"get");


    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getPermissions(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getPermissions(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getPermissions(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getPermissions(data,type) {
    MyAjax("/permissions",data,type,(res)=>{
        USERS=res.data.list
        CURRENT_PAGE=res.currentPage
        $("#pageSize").val(res.pageSize);
        $("#current_page").val(res.currentPage);
        $("#all_page").text(res.data.count)
        COUNT=res.count
        packaging(res.data.list)
        // loadLeaderToDiv(res.leaders)
    })
}

function getLeader(code) {
    MyAjax(
        "/getUserByTeam",
        {code:code},
        "get",
        (res)=>{
            loadMemberToDiv(res.leaders)
        }
        )
}

function loadMemberToDiv(data) {
    let htmlStr_add=""
    $.each(data,(i,user)=>{
        htmlStr_add+=`
            <option value="${user.code}" name="${user.name}">${user.name}</option>
        `
    })
    $("#teamLeader").html(htmlStr_add)
}

function loadLeaderToDiv(data) {
    let htmlStr_search="<option value='all'>全部</option>"
    $.each(data,(i,user)=>{
        htmlStr_search+=`
            <option value="${user.code}" name="${user.name}">${user.name}</option>
        `
    })
    $("#search_teamLeader").html(htmlStr_search)
}

//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val(),
    }
    return search
}

function doSearch() {
    getPermissions(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,per)=>{
        htmlStr+=`
                <tr>
                <td>${per.id}</td>
                <td>${per.code}</td>
                <td>${per.name}</td>
                <td>${per.desp}</td>
                <td>${per.enable==1?'正常':'已解散'}</td>
                <td>${per.updateTime}</td>
                <td>${per.createTime}</td>
                <td>
                <div onclick="switchBtn(2);getLeader('${per.id}');getUserById(${per.id})">修改</div>
                <div onclick="delUser('${per.id}')">删除</div>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}

function getUserById(id) {
    MyAjax("/permissionId/"+id,"","get",(res)=>{
        setFormData(res.data)
    })
}

//设置表单数据
function setFormData(per) {

    $("#id").val(per.id)
    $("#code").val(per.code)
    $("#name").val(per.name)
    $("#desp").val(per.desp)
    if(per.enable=='0')
        $("#enable_no").prop("checked","true")
    else
        $("#enable_yes").prop("checked","true")
}

//获取表单提交的数据
function getUserFormData() {
    let team={
        id:$("#id").val(),
        code:$("#code").val(),
        name:$("#name").val(),
        desp:$("#desp").val(),
        enable:getEnable()
    }
    return team;

}

//删除记录
function delUser(id) {
    if(!confirm("是否删除?"))
        return
    MyAjax("/permission/"+id,"","Delete",(res)=>{
        if(res.result==0){
            alert("小组成员未解散,不能删除该小组！")
        }
        else if(res.data>0){
            // console.log("删除成功")
            getPermissions(getSearchData(),"get");
        }else {
            alert("删除失败，请重试！")
        }
    })
}

//修改记录
function modify() {
    let team=getUserFormData();
    MyAjax("/permission",JSON.stringify(team),"post",(res)=>{
        if(res.data>0){
            $("#add_div").hide();
            remove_pointer();
            resetFormData();
            getPermissions(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}

//添加记录
function add() {
    MyAjax("/permission",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.data>0){
            $("#add_div").hide();
            getPermissions(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}


//重置表单数据为空
function resetFormData() {
    $("#id").val("")
    $("#code").val("")
    $("#name").val("")
    $("#desp").val("")
    $("#enable").val("")
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}



//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getPermissions(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getPermissions(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getPermissions(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getPermissions(search,"get");
}
