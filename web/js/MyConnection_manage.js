$(function () {
    getConnections(getSearchData(),"get");


    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getConnections(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getConnections(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getConnections(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getConnections(data,type) {
    MyAjax("/collections",data,type,(res)=>{
        // USERS=res.teams
        CURRENT_PAGE=res.data.currentPage
        $("#pageSize").val(res.data.pageSize);
        $("#current_page").val(res.data.currentPage);
        $("#all_page").text(res.data.count);
        COUNT=res.data.count
        packaging(res.data.list)
        // loadLeaderToDiv(res.leaders)
    })
}

//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        userCode:sessionStorage.getItem("userId")
    }
    return search
}

function doSearch() {
    getConnections(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,word)=>{
        htmlStr+=`
                <tr>
                    <td>${word.id}</td>
                    <td>${word.word.english}</td>
                    <td>${word.word.chinese}</td>
                    <td>${word.word.giveLikeNum}</td>
                    <td>${word.createTime}</td>
                    <td>
                    <div onclick="quitCollection('${word.word.code}')">取消收藏</div>
                    </td>
                </tr>           
            `
    })
    $("#tbody").html(htmlStr)
}

function quitCollection(enCode) {
    let data={
        word:{code:enCode},
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/collection",JSON.stringify(data),"DELETE",(res)=>{
        if(res.data>0){
            getConnections(getSearchData(),"get")
        }
    })
}


//添加记录
function downloadWordList() {
    MyAjax("/downLoadWordList",JSON.stringify(getSearchData()),"POST",(res)=>{
        if(res.data>0){
            alert("已保存！")
        }else {
            alert("请稍后再试！")
        }
    })
}

//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getConnections(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getConnections(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getConnections(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getConnections(search,"get");
}
