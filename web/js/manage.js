$(function () {
    getsetLoginData()
    $("#content").load("/html/user_manage.html")
    DivHoverChangeColor($("li[onclick]"),"#00BCD4")
})

//改变当前单击元素的背景颜色
function DivHoverChangeColor(element,color) {
    // var $jiami = $(element)
    $.each(element,(i,v)=>{
        element[i].index=i;
        element[i].addEventListener("click",(e)=> {
            $.each(element, (i, v) => {
                element[i].style.background = ""
            })
            var $1 = e.currentTarget;
            $1.style.background = color
        })
    })
}

function getsetLoginData() {
    let userId=sessionStorage.getItem("userId")
    if(userId==null || userId=='')
    location.href='/html/login.html'
    MyAjax("/userByCode/"+userId,"","get",(res)=>{
        $("#user_name").text(res.data.name)
        $("#team_name").text(res.data.groupCode.teamName)
        $("#team_code").text(res.data.groupCode.teamCode)
    })
}
function showContent(url) {
    $("#content").load(url);
}

function isLeader() {
    MyAjax(
        "/isLeader",
        {code:sessionStorage.getItem("userId")},
        "get",
        (res)=>{
            if(!res.data.result>0){
                alert("抱歉，您没有权限！")
            }else {
                showContent('/html/sign_index_manage.html')
            }
        }
    )
}

function quit() {
    sessionStorage.removeItem("logindata")
    $.cookie("token",null)
    location.href="/html/login.html"
}

function toggleMenu() {
    var width = $("#left").width();
    if(width==0){
        $("#left").width("15%");
        $("#right").width("85%");
        $("div#back_btn").css("left","calc(100% - 10px)")
    }else {
        $("#left").width(0);
        $("#right").width("100%");
        $("div#back_btn").css("left",0)

    }

}