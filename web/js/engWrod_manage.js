$(function () {
    getEngWOrds(getSearchData(),"get");


    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getEngWOrds(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getEngWOrds(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getEngWOrds(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getEngWOrds(data,type) {
    MyAjax("/engWrods",data,type,(res)=>{
        // USERS=res.teams
        CURRENT_PAGE=res.data.currentPage
        $("#pageSize").val(res.data.pageSize);
        $("#current_page").val(res.data.currentPage);
        $("#all_page").text(res.data.count);
        COUNT=res.data.count
        packaging(res.data.list)
        // loadLeaderToDiv(res.leaders)
    })
}

//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val()
    }
    return search
}

function doSearch() {
    getEngWOrds(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,word)=>{
        htmlStr+=`
                <tr>
                <td>${word.id}</td>
                <td>${word.english}</td>
                <td>${word.chinese}</td>
                <td>${word.giveLikeNum}</td>
                <td>${word.createTime}</td>
                <td>
                <div  id ="giveLike${word.code}"  onclick="giveLike('${word.code}')">点赞</div>
                <div  id ="quitGiveLike${word.code}" onclick="quitGiveLike('${word.code}')">取消点赞</div>
                <div  id ="collection${word.code}" onclick="getCollection('${word.code}')">收藏</div>
                <div  id ="quitCollection${word.code}" onclick="quitCollection('${word.code}')">取消收藏</div>
                </td>
</tr>
            `

        isGiveLike(word.code,(res)=>{
            if(res.data>0){
                $("#quitGiveLike"+word.code).show();
                $("#giveLike"+word.code).hide();
            }else {
                $("#quitGiveLike"+word.code).hide();
                $("#giveLike"+word.code).show();
            }
        })
        isConnect(word.code,(res)=>{
            if(res.data>0){
                $("#quitCollection"+word.code).show();
                $("#collection"+word.code).hide();
            }else {
                $("#quitCollection"+word.code).hide();
                $("#collection"+word.code).show();
            }
        })

    })
    $("#tbody").html(htmlStr)
}
function isGiveLike(enCode,callback) {
    let data={
        engWordCode:enCode,
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/isGiveLike",data,"get",(res)=>{
        callback(res)
    })
}

function isConnect(enCode,callback) {
    let data={
        word:{code:enCode},
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/isConnection",JSON.stringify(data),"post",(res)=>{
        callback(res)
    })
}


function giveLike(enCode) {
    let data={
        engWordCode:enCode,
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/giveLike",JSON.stringify(data),"post",(res)=>{
        if(res.data>0){
            $("#quitGiveLike"+enCode).show();
            $("#giveLike"+enCode).hide();
        }

    })
}
function quitGiveLike(enCode) {
    let data={
        engWordCode:enCode,
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/giveLike",JSON.stringify(data),"DELETE",(res)=>{
        if(res.data>0){
            $("#quitGiveLike"+enCode).hide();
            $("#giveLike"+enCode).show();
        }
    })
}

function getCollection(enCode) {
    let data={
        word:{code:enCode},
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/collection",JSON.stringify(data),"post",(res)=>{
        if(res.data>0){
            $("#quitCollection"+enCode).show();
            $("#collection"+enCode).hide();
        }
    })
}

function quitCollection(enCode) {
    let data={
        word:{code:enCode},
        userCode:sessionStorage.getItem("userId")
    }
    MyAjax("/collection",JSON.stringify(data),"DELETE",(res)=>{
        if(res.data>0){
            $("#quitCollection"+enCode).hide();
            $("#collection"+enCode).show();
        }
    })
}


//添加记录
function add() {
    MyAjax("/engWord",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.data>0){
            $("#add_div").hide();
            getEngWOrds(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}

//获取表单提交的数据
function getUserFormData() {
    let word={
        chinese:$("#chinese").val(),
        english:$("#enWord").val(),
        userCode:sessionStorage.getItem("userId")
    }
    return word;

}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//重置表单数据为空
function resetFormData() {
    $("#id").val("")
    $("#enWord").val("")
    $("#chinese").val("")
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}



//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getEngWOrds(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getEngWOrds(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getEngWOrds(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getEngWOrds(search,"get");
}
