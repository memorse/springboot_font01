$(function () {
    getDailyReports(getSearchData(),"get");
    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getDailyReports(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getDailyReports(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getDailyReports(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getDailyReports(data,type) {
    MyAjax("/userDailyReports",data,type,(res)=>{
        // USERS=res.teams
        // CURRENT_PAGE=res.currentPage
        // $("#pageSize").val(res.pageSize);
        // $("#current_page").val(res.currentPage);
        // $("#all_page").text(res.);d
        // COUNT=res.count
        packaging(res.data)
        // loadLeaderToDiv(res.leaders)
    })
}

//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val(),
        userCode:sessionStorage.getItem("userId")
    }
    return search
}

function doSearch() {
    getDailyReports(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,report)=>{
        htmlStr+=`
                <tr>
                <td>${report.id}</td>
                <td>${report.name}</td>
                <td>${report.teacher}</td>
                <td>${report.coatch}</td>
                <td>${report.degree}</td>
                <td>${report.gain}</td>
                <td>${report.deficiency}</td>
                <td>${report.enable==2?'已查看':report.enable==1?"已提交":"草稿"}</td>
                <td>${report.dateTime}</td>
                <td>${report.createTime}</td>
                <td>
                <div onclick="switchBtn(2);getReportById(${report.id})">修改</div>
                <div onclick="delReport(${report.id})">删除</div>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}

function getReportById(id) {
    MyAjax("/dailyReport/"+id,"","get",(res)=>{
        setFormData(res.data)
    })
}
//设置表单数据
function setFormData(report) {

    $("#id").val(report.id)
    $("#name").val(report.name)
    $("#teacher").val(report.teacher)
    $("#coatch").val(report.coatch)
    $("#degree").val(report.degree)
    $("#gain").val(report.gain)
    $("#deficiency").val(report.deficiency)
    if(report.enable=='0')
        $("#enable_0").prop("checked","true")
    else if (report.enable=='1')
        $("#enable_1").prop("checked","true")
}

function downloadReport(id) {
    MyAjax("/downloadReport/"+id,"","post",(res)=>{
        console.log(res.message)
    })
}


//修改记录
function modify() {
    let report=getUserFormData();
    MyAjax("/dailyReport",JSON.stringify(report),"post",(res)=>{
        if(res.data>0){
            $("#add_div").hide();
            remove_pointer();
            resetFormData();
            getDailyReports(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}


//删除记录
function delReport(id) {
    if(!confirm("是否删除?"))
        return
    MyAjax("/dailyReport/"+id,"","Delete",(res)=>{
        if(res.data>0){
            console.log("删除成功")
            getDailyReports(getSearchData(),"get");
        }else {
            console.log("删除失败")
        }
    })
}

//获取表单提交的数据
function getUserFormData() {
    let report={
        id:$("#id").val(),
        name:$("#name").val(),
        teacher:$("#teacher").val(),
        coatch:$("#coatch").val(),
        degree:$("#degree").val(),
        gain:$("#gain").val(),
        deficiency:$("#deficiency").val(),
        enable:getEnable(),
        userCode: sessionStorage.getItem("userId")
    }
    return report;

}

//添加记录
function add() {
    MyAjax("/dailyReport",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.data>0){
            $("#add_div").hide();
            getDailyReports(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}


//重置表单数据为空
function resetFormData() {
    $("#id").val("")
    $("#name").val("")
    $("#teacher").val("")
    $("#coatch").val("")
    $("#degree").val("")
    $("#gain").val("")
    $("#deficiency").val("")
    $("#enable").val("")
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}



//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getDailyReports(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getDailyReports(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getDailyReports(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getDailyReports(search,"get");
}
