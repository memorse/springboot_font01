var URL="http://localhost:8802";
function uploadImg(id) {
    var resultFile = $("#uploadFile")[0].files[0];
    var formData=new FormData()
    formData.append('file',resultFile)
    $.ajax({
        url:URL+"/uploadImg",
        data:formData,
        dataType:"json",
        type:'post',
        contentType:false,
        processData:false,
        xhrFields: {
            withCredentials: true //允许跨域带Cookie
        },
        success:(res)=>{
        console.log(res)
        if(res==""){
        $("#img_tip_0").css("display","inline-block");
        $("#img_tip_1").css("display","none");
    }else {
        $("#img_tip_1").css("display","inline-block");
        $("#img_tip_0").css("display","none");
        $("#"+id).val(res.url)
    }
},
    error:(err)=>{
        console.log(err)
    }
})
}
function uploadImg2(id,fileId,fileIndex) {
    var resultFile = $("#"+fileId)[0].files[0];
    var formData=new FormData()
    formData.append('file',resultFile)
    $.ajax({
        url:URL+"/uploadImg",
        data:formData,
        dataType:"json",
        type:'post',
        xhrFields: {
            withCredentials: true //允许跨域带Cookie
        },
        contentType:false,
        processData:false,
        success:(res)=>{
        console.log(res)
        if(res==""){
            if(fileIndex==1){
                $("#img_tip_0_1").css("display","inline-block");
                $("#img_tip_1_1").css("display","none");
            }else if(fileIndex==2) {
                $("#img_tip_0_2").css("display","inline-block");
                $("#img_tip_1_2").css("display","none");
            }

    }else {
            if(fileIndex==1){
                $("#img_tip_1_1").css("display","inline-block");
                $("#img_tip_0_1").css("display","none");
            }else if(fileIndex==2) {
                $("#img_tip_1_2").css("display","inline-block");
                $("#img_tip_0_2").css("display","none");
            }
            $("#"+id).val(res.url)
    }
},
    error:(err)=>{
        console.log(err)
    }
})
}

function MyAjax(url,data,type,callback) {
    let user=$.cookie("token")
    $.ajax({
        url:URL+url,
        type:type,
        contentType:"application/json; charset=utf-8",
        headers:{
            "Authorization":$.cookie("token")
        },
        dataType:"json",
        data:data,
        xhrFields: {
            withCredentials: true //允许跨域带Cookie
        },
        success:function (res) {
            AssertToken(res)
            callback(res);
            // console.log(res)
        },
        error:function (err) {
            // console.log(err)
        }
    })
}

function MyListAjax(url,data,type,callback) {
    let user=$.cookie("token")
    $.ajax({
        url:URL+url,
        type:type,
        headers:{
            "Authorization":$.cookie("token")
        },
        dataType:"json",
        contentType:"application/json; charset=utf-8",
        data:data,
        xhrFields: {
            withCredentials: true //允许跨域带Cookie
        },
        success:function (res) {
            AssertToken(res)
            callback(res);
            // console.log(res)
        },
        error:function (err) {
            // console.log(err)
        }
    })
}

function AssertToken(data) {
    if(data.code==403)
        $("#content").html("<h1>"+data.message+"</h1>")
    if (data.code==401)
        location.href="/html/login.html"
}

function AssertTokenWithAlert(data) {
    if(data.code==403)
        alert(data.message)
    if (data.code==401)
        location.href="/html/login.html"
}

//获取搜索性别条件
function getGender() {
    return $('input:radio[name="gender"]:checked').val();
}