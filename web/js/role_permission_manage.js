$(function () {
    getUsers(getSearchData(),"get");
    getPermissions();

    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getUsers(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getUsers(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getUsers(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getUsers(data,type) {
    MyAjax("/role_permission",data,type,(res)=>{
        USERS=res.data.list
        CURRENT_PAGE=res.data.currentPage
        $("#pageSize").val(res.data.pageSize);
        $("#current_page").val(res.data.currentPage);
        $("#all_page").text(res.data.count);
        COUNT=res.data.count
        packaging(res.data.list)
    })
}

function getPermissions() {
    MyAjax("/permissions","","get",(res)=>{
        loadPermissionToDiv(res.data.list)
    })
}

//加载所有权限
function loadPermissionToDiv(data) {
    let str=""
    $.each(data,(i,per)=>{
       str+= `
           <div>
           <input type="checkbox" id="${per.code}" name="permission" value="${per.code}">
           <label for="${per.code}">${per.name}</label>
           </div>   
        `
    })
    $("#permissions").html(str)
}
//获取查询条件
function getSearchData() {
    let search={
        name:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        degree:$("#search_degree").val(),
        gender:$('input:radio[name="search_gender"]:checked').val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val(),
        status:$("#search_status").val(),
        team:$("#search_team").val()
    }
    return search
}

function doSearch() {
    getUsers(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,user)=>{
        htmlStr+=`
                <tr>
                <td>${user.id}</td>
                <td>${user.roleCode}</td>
                <td>${user.roleName}</td>
                <td>${user.permissions==null?'暂无权限':showPermission(user.permissions.split(','))}</td>
                <td>${user.createTime}</td>
                <td>${user.updateTime}</td>
                <td>
                <div onclick="switchBtn(2);getPermissionByRoleCode('${user.roleCode}')">修改</div>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}

function showPermission(data) {
    let str=""
    $.each(data,(i,per)=>{
        if (i==data.length-1)
            str+=per
        else
            str+=per+" | "
    })
    return str
}
//获取表单提交的数据
function getUserFormData() {
    let user={
        id:$("#id").val(),
        roleName:$("#name").val(),
        roleCode:$("#code").val(),
        permissions:getPerMissionFromData()
    }
    return user;

}

//删除记录
function delUser(id) {
    if(!confirm("是否删除?"))
        return
    MyAjax("/user/"+id,"","Delete",(res)=>{
        if(res.data.result>0){
            console.log("删除成功")
            getUsers(getSearchData(),"get");
        }else {
            console.log("删除失败")
        }
    })
}

//修改记录
function modify() {
    let user=getUserFormData();
    MyAjax("/role_permission",JSON.stringify(user),"post",(res)=>{
        if(res.data>0){
            $("#add_div").hide();
            remove_pointer();
            resetFormData();
            getUsers(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}

//添加记录
function add() {
    MyAjax("/user",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.data.result>0){
            $("#add_div").hide();
            getUsers(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

function loadTeamDataToHtml(data) {
    let htmlStr="";
    let htmlStr_search=`<option value="all">全部</option>`
    $.each(data,(i,team)=>{
        htmlStr+=`
            <option value="${team.teamCode}">${team.teamName}</option>
        `
        htmlStr_search+=`
            <option value="${team.teamCode}">${team.teamName}</option>
        `
    })
    $("#group_code").html(htmlStr)
    $("#search_team").html(htmlStr_search)
}

//加密
function jiami(str) {
    let result="";
    if(str=='' ||str==null)
        return result;
    for(i=0;i<str.length;i++){
        result+=str[i].replace(str[i],"*")
    }
    return result;
}


//获取权限
function getPerMissionFromData() {
    let pers = $("input:checkbox[name=permission]");
    let per = "";
    for (let i = 0; i < pers.length; i++) {
        if (pers[i].checked) {
            per += pers[i].value + "::1,"
        }else {
            per += pers[i].value + "::0,"
        }
    }
    return per;
}



//重置表单数据为空
function resetFormData() {
    $("#id").val("")
    $("#name").val("")
    $("#code").val("")
    let pers = $("input:checkbox[name=permission]");
    for (let i = 0; i < pers.length; i++) {
        pers[i].checked=false
    }

}

//设置表单数据
function setFormData(pmss) {

       $.each(pmss,(i,pms)=>{
           $("input:checkbox[value="+pms+"]").prop("checked",true)
       })
}

function getPermissionByRoleCode(myRoleCode) {

    MyAjax("/role_permission/"+myRoleCode,"","get",(res)=>{
        $("#name").val(res.data[0].roleName)
        $("#code").val(res.data[0].roleCode)
        $("#id").val(res.data[0].id)
        setFormData(res.data[0].permissions==null?"":res.data[0].permissions.split(","))
    })
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getUsers(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getUsers(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getUsers(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getUsers(search,"get");
}
